package com.devcamp.employeerestapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class HomeController {
    @GetMapping("/employees")
    public ArrayList<Employee> employeeList() {
        Employee employee1 = new Employee(1, "Nguyen Van", "A", 1000);
        Employee employee2 = new Employee(2, "Nguyen Huynh", "B", 2000);
        Employee employee3 = new Employee(3, "Nguyen Pham", "C", 2500);
        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(employee1);
        employees.add(employee2);
        employees.add(employee3);
        return employees;

    }

}
